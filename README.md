# Shopping list

Extend the builtin shopping list components to be able to manage quantities
in addition to the items

## Installation

1. Clone the repository within `custom_components`
```shell
cd /config/custom_components
git clone https://gitlab.com/zeronounours/ha_custom_component_shopping_list.git shopping_list
```
2. Restart Home Assistant

## Configuration

The configuration of the component is the same as the builtin `shopping_list`
integration: https://www.home-assistant.io/integrations/shopping_list/
