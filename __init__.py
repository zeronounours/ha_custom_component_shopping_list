"""Support to manage a shopping list."""
from http import HTTPStatus
import logging
import uuid

import voluptuous as vol

from homeassistant import config_entries
from homeassistant.components import frontend, http, websocket_api
from homeassistant.components.http.data_validator import RequestDataValidator
from homeassistant.config_entries import ConfigEntry
from homeassistant.const import ATTR_NAME
from homeassistant.core import HomeAssistant, ServiceCall, callback
import homeassistant.helpers.config_validation as cv
from homeassistant.helpers.typing import ConfigType
from homeassistant.util.json import load_json, save_json

from .const import (
    DOMAIN,
    SERVICE_ADD_ITEM,
    SERVICE_CLEAR_COMPLETED_ITEMS,
    SERVICE_COMPLETE_ALL,
    SERVICE_COMPLETE_ITEM,
    SERVICE_INCOMPLETE_ALL,
    SERVICE_INCOMPLETE_ITEM,
)

ATTR_COMPLETE = "complete"
ATTR_UNIT = "unit"
ATTR_QUANTITY = "quantity"

_LOGGER = logging.getLogger(__name__)
CONFIG_SCHEMA = vol.Schema({DOMAIN: {}}, extra=vol.ALLOW_EXTRA)
EVENT = "shopping_list_updated"
ITEM_UPDATE_SCHEMA = vol.Schema(
    {
        ATTR_COMPLETE: bool,
        ATTR_NAME: str,
        ATTR_UNIT: str,
        ATTR_QUANTITY: cv.positive_float,
    },
)
PERSISTENCE = ".shopping_list.json"

SERVICE_ITEM_SCHEMA = vol.Schema({vol.Required(ATTR_NAME): vol.Any(None, cv.string)})
SERVICE_ITEM_ADD_SCHEMA = vol.Schema(
    {
        vol.Required(ATTR_NAME): vol.Any(None, cv.string),
        vol.Inclusive(ATTR_UNIT, "quantity"): str,
        vol.Inclusive(ATTR_QUANTITY, "quantity"): cv.positive_float,
    },
)
SERVICE_LIST_SCHEMA = vol.Schema({})

WS_TYPE_SHOPPING_LIST_ITEMS = "shopping_list/items"
WS_TYPE_SHOPPING_LIST_ADD_ITEM = "shopping_list/items/add"
WS_TYPE_SHOPPING_LIST_UPDATE_ITEM = "shopping_list/items/update"
WS_TYPE_SHOPPING_LIST_CLEAR_ITEMS = "shopping_list/items/clear"

SCHEMA_WEBSOCKET_ITEMS = websocket_api.BASE_COMMAND_MESSAGE_SCHEMA.extend(
    {vol.Required("type"): WS_TYPE_SHOPPING_LIST_ITEMS}
)

SCHEMA_WEBSOCKET_ADD_ITEM = websocket_api.BASE_COMMAND_MESSAGE_SCHEMA.extend(
    {
        vol.Required("type"): WS_TYPE_SHOPPING_LIST_ADD_ITEM,
        vol.Required("name"): str,
        vol.Inclusive("unit", "quantity"): str,
        vol.Inclusive("quantity", "quantity"): cv.positive_float,
    }
)

SCHEMA_WEBSOCKET_UPDATE_ITEM = websocket_api.BASE_COMMAND_MESSAGE_SCHEMA.extend(
    {
        vol.Required("type"): WS_TYPE_SHOPPING_LIST_UPDATE_ITEM,
        vol.Required("item_id"): str,
        vol.Optional("name"): str,
        vol.Optional("unit"): str,
        vol.Optional("quantity"): cv.positive_float,
        vol.Optional("complete"): bool,
    }
)

SCHEMA_WEBSOCKET_CLEAR_ITEMS = websocket_api.BASE_COMMAND_MESSAGE_SCHEMA.extend(
    {vol.Required("type"): WS_TYPE_SHOPPING_LIST_CLEAR_ITEMS}
)


async def async_setup(hass: HomeAssistant, config: ConfigType) -> bool:
    """Initialize the shopping list."""

    if DOMAIN not in config:
        return True

    hass.async_create_task(
        hass.config_entries.flow.async_init(
            DOMAIN, context={"source": config_entries.SOURCE_IMPORT}
        )
    )

    return True


async def async_setup_entry(hass: HomeAssistant, config_entry: ConfigEntry) -> bool:
    """Set up shopping list from config flow."""

    async def add_item_service(call: ServiceCall) -> None:
        """Add an item with `name`."""
        data = hass.data[DOMAIN]
        if (name := call.data.get(ATTR_NAME)) is not None:
            kargs = {"name": name}
            if ATTR_UNIT in call.data:
                kargs["unit"] = call.data[ATTR_UNIT]
            if ATTR_QUANTITY in call.data:
                kargs["quantity"] = call.data[ATTR_QUANTITY]
            await data.async_add(**kargs)
            hass.bus.async_fire(EVENT)

    async def complete_item_service(call: ServiceCall) -> None:
        """Mark the item provided via `name` as completed."""
        data = hass.data[DOMAIN]
        if (name := call.data.get(ATTR_NAME)) is None:
            return
        try:
            items = [item for item in data.items if item["name"] == name]
        except IndexError:
            _LOGGER.error("Removing of item failed: %s cannot be found", name)
        else:
            for item in items:
                await data.async_update(item["id"], {"complete": True})
            hass.bus.async_fire(EVENT)

    async def incomplete_item_service(call: ServiceCall) -> None:
        """Mark the item provided via `name` as incomplete."""
        data = hass.data[DOMAIN]
        if (name := call.data.get(ATTR_NAME)) is None:
            return
        try:
            items = [item for item in data.items if item["name"] == name]
        except IndexError:
            _LOGGER.error("Restoring of item failed: %s cannot be found", name)
        else:
            for item in items:
                await data.async_update(item["id"], {"complete": False})
            hass.bus.async_fire(EVENT)

    async def complete_all_service(call: ServiceCall) -> None:
        """Mark all items in the list as complete."""
        await data.async_update_list({"complete": True})
        hass.bus.async_fire(EVENT)

    async def incomplete_all_service(call: ServiceCall) -> None:
        """Mark all items in the list as incomplete."""
        await data.async_update_list({"complete": False})
        hass.bus.async_fire(EVENT)

    async def clear_completed_items_service(call: ServiceCall) -> None:
        """Clear all completed items from the list."""
        await data.async_clear_completed()
        hass.bus.async_fire(EVENT)

    data = hass.data[DOMAIN] = ShoppingData(hass)
    await data.async_load()

    hass.services.async_register(
        DOMAIN, SERVICE_ADD_ITEM, add_item_service, schema=SERVICE_ITEM_ADD_SCHEMA
    )
    hass.services.async_register(
        DOMAIN, SERVICE_COMPLETE_ITEM, complete_item_service, schema=SERVICE_ITEM_SCHEMA
    )
    hass.services.async_register(
        DOMAIN,
        SERVICE_INCOMPLETE_ITEM,
        incomplete_item_service,
        schema=SERVICE_ITEM_SCHEMA,
    )
    hass.services.async_register(
        DOMAIN,
        SERVICE_COMPLETE_ALL,
        complete_all_service,
        schema=SERVICE_LIST_SCHEMA,
    )
    hass.services.async_register(
        DOMAIN,
        SERVICE_INCOMPLETE_ALL,
        incomplete_all_service,
        schema=SERVICE_LIST_SCHEMA,
    )
    hass.services.async_register(
        DOMAIN,
        SERVICE_CLEAR_COMPLETED_ITEMS,
        clear_completed_items_service,
        schema=SERVICE_LIST_SCHEMA,
    )

    hass.http.register_view(ShoppingListView)
    hass.http.register_view(CreateShoppingListItemView)
    hass.http.register_view(UpdateShoppingListItemView)
    hass.http.register_view(ClearCompletedItemsView)

    frontend.async_register_built_in_panel(
        hass, "shopping-list", "shopping_list", "mdi:cart"
    )

    websocket_api.async_register_command(
        hass,
        WS_TYPE_SHOPPING_LIST_ITEMS,
        websocket_handle_items,
        SCHEMA_WEBSOCKET_ITEMS,
    )
    websocket_api.async_register_command(
        hass,
        WS_TYPE_SHOPPING_LIST_ADD_ITEM,
        websocket_handle_add,
        SCHEMA_WEBSOCKET_ADD_ITEM,
    )
    websocket_api.async_register_command(
        hass,
        WS_TYPE_SHOPPING_LIST_UPDATE_ITEM,
        websocket_handle_update,
        SCHEMA_WEBSOCKET_UPDATE_ITEM,
    )
    websocket_api.async_register_command(
        hass,
        WS_TYPE_SHOPPING_LIST_CLEAR_ITEMS,
        websocket_handle_clear,
        SCHEMA_WEBSOCKET_CLEAR_ITEMS,
    )

    websocket_api.async_register_command(hass, websocket_handle_reorder)

    return True


METRIC_PREFIX_CONVERSION = {
    "k": 1000,
    "h": 100,
    "da": 10,
    "": 1,
    "d": 0.1,
    "c": 0.01,
    "m": 0.001,
}


class ShoppingDataEntry(dict):
    """Class to manage entries of the shopping list."""

    def __init__(self, *args, **kargs):
        super().__init__(*args, **kargs)
        self.setdefault("unit", "")
        self.setdefault("quantity", 1.0)
        self["_prefix_unit"] = ""
        for k in METRIC_PREFIX_CONVERSION.keys():
            if self["unit"].startswith(k) and k != "":
                self["_prefix_unit"] = k
                break
        self["_base_unit"] = self["unit"][len(self["_prefix_unit"]):].strip()

    def add(self, entry):
        if (
            entry["name"] == self["name"]
            and entry["_base_unit"] == self["_base_unit"]
        ):
            self["quantity"] += (
                entry["quantity"]
                * METRIC_PREFIX_CONVERSION[entry["_prefix_unit"]]
                / METRIC_PREFIX_CONVERSION[self["_prefix_unit"]]
            )
            return True
        return False


class ShoppingData:
    """Class to hold shopping list data."""

    def __init__(self, hass):
        """Initialize the shopping list."""
        self.hass = hass
        self.items = []

    async def async_add(self, name, quantity=1, unit=""):
        """Add a shopping list item."""
        entry = ShoppingDataEntry(
            name=name,
            quantity=float(quantity),
            unit=unit,
            id=uuid.uuid4().hex,
            complete=False,
        )
        for itm in self.items:
            if not itm["complete"] and itm.add(entry):
                item = itm
                break
        else:
            item = entry
            self.items.append(item)
        await self.hass.async_add_executor_job(self.save)
        return item

    async def async_update(self, item_id, info):
        """Update a shopping list item."""
        item = next((itm for itm in self.items if itm["id"] == item_id), None)

        if item is None:
            raise KeyError

        info = ITEM_UPDATE_SCHEMA(info)
        item.update(info)
        await self.hass.async_add_executor_job(self.save)
        return item

    async def async_clear_completed(self):
        """Clear completed items."""
        self.items = [itm for itm in self.items if not itm["complete"]]
        await self.hass.async_add_executor_job(self.save)

    async def async_update_list(self, info):
        """Update all items in the list."""
        for item in self.items:
            item.update(info)
        await self.hass.async_add_executor_job(self.save)
        return self.items

    @callback
    def async_reorder(self, item_ids):
        """Reorder items."""
        # The array for sorted items.
        new_items = []
        all_items_mapping = {item["id"]: item for item in self.items}
        # Append items by the order of passed in array.
        for item_id in item_ids:
            if item_id not in all_items_mapping:
                raise KeyError
            new_items.append(all_items_mapping[item_id])
            # Remove the item from mapping after it's appended in the result array.
            del all_items_mapping[item_id]
        # Append the rest of the items
        for key in all_items_mapping:
            # All the unchecked items must be passed in the item_ids array,
            # so all items left in the mapping should be checked items.
            if all_items_mapping[key]["complete"] is False:
                raise vol.Invalid(
                    "The item ids array doesn't contain all the unchecked shopping list items."
                )
            new_items.append(all_items_mapping[key])
        self.items = new_items
        self.hass.async_add_executor_job(self.save)

    async def async_load(self):
        """Load items."""

        def load():
            """Load the items synchronously."""
            return load_json(self.hass.config.path(PERSISTENCE), default=[])

        self.items = [
            ShoppingDataEntry(i)
            for i in await self.hass.async_add_executor_job(load)
        ]

    def save(self):
        """Save the items."""
        save_json(
            self.hass.config.path(PERSISTENCE),
            [
                {k: v for k, v in i.items() if not k.startswith("_")}
                for i in self.items
            ],
        )


class ShoppingListView(http.HomeAssistantView):
    """View to retrieve shopping list content."""

    url = "/api/shopping_list"
    name = "api:shopping_list"

    @callback
    def get(self, request):
        """Retrieve shopping list items."""
        return self.json(request.app["hass"].data[DOMAIN].items)


class UpdateShoppingListItemView(http.HomeAssistantView):
    """View to retrieve shopping list content."""

    url = "/api/shopping_list/item/{item_id}"
    name = "api:shopping_list:item:id"

    async def post(self, request, item_id):
        """Update a shopping list item."""
        data = await request.json()

        try:
            item = await request.app["hass"].data[DOMAIN].async_update(item_id, data)
            request.app["hass"].bus.async_fire(EVENT)
            return self.json(item)
        except KeyError:
            return self.json_message("Item not found", HTTPStatus.NOT_FOUND)
        except vol.Invalid:
            return self.json_message("Item not found", HTTPStatus.BAD_REQUEST)


class CreateShoppingListItemView(http.HomeAssistantView):
    """View to retrieve shopping list content."""

    url = "/api/shopping_list/item"
    name = "api:shopping_list:item"

    @RequestDataValidator(vol.Schema({
        vol.Required("name"): str,
        vol.Required("quantity", default=1): cv.positive_float,
        vol.Required("unit", default=""): str,
    }))
    async def post(self, request, data):
        """Create a new shopping list item."""
        item = await request.app["hass"].data[DOMAIN].async_add(
            data["name"],
            data["quantity"],
            data["unit"],
        )
        request.app["hass"].bus.async_fire(EVENT)
        return self.json(item)


class ClearCompletedItemsView(http.HomeAssistantView):
    """View to retrieve shopping list content."""

    url = "/api/shopping_list/clear_completed"
    name = "api:shopping_list:clear_completed"

    async def post(self, request):
        """Retrieve if API is running."""
        hass = request.app["hass"]
        await hass.data[DOMAIN].async_clear_completed()
        hass.bus.async_fire(EVENT)
        return self.json_message("Cleared completed items.")


@callback
def websocket_handle_items(hass, connection, msg):
    """Handle get shopping_list items."""
    connection.send_message(
        websocket_api.result_message(msg["id"], hass.data[DOMAIN].items)
    )


@websocket_api.async_response
async def websocket_handle_add(hass, connection, msg):
    """Handle add item to shopping_list."""
    item = await hass.data[DOMAIN].async_add(
        msg["name"],
        *(msg[k] for k in ("quantity", "unit") if "quantity" in msg),
    )
    hass.bus.async_fire(EVENT, {"action": "add", "item": item})
    connection.send_message(websocket_api.result_message(msg["id"], item))


@websocket_api.async_response
async def websocket_handle_update(hass, connection, msg):
    """Handle update shopping_list item."""
    msg_id = msg.pop("id")
    item_id = msg.pop("item_id")
    msg.pop("type")
    data = msg

    try:
        item = await hass.data[DOMAIN].async_update(item_id, data)
        hass.bus.async_fire(EVENT, {"action": "update", "item": item})
        connection.send_message(websocket_api.result_message(msg_id, item))
    except KeyError:
        connection.send_message(
            websocket_api.error_message(msg_id, "item_not_found", "Item not found")
        )


@websocket_api.async_response
async def websocket_handle_clear(hass, connection, msg):
    """Handle clearing shopping_list items."""
    await hass.data[DOMAIN].async_clear_completed()
    hass.bus.async_fire(EVENT, {"action": "clear"})
    connection.send_message(websocket_api.result_message(msg["id"]))


@websocket_api.websocket_command(
    {
        vol.Required("type"): "shopping_list/items/reorder",
        vol.Required("item_ids"): [str],
    }
)
def websocket_handle_reorder(hass, connection, msg):
    """Handle reordering shopping_list items."""
    msg_id = msg.pop("id")
    try:
        hass.data[DOMAIN].async_reorder(msg.pop("item_ids"))
        hass.bus.async_fire(EVENT, {"action": "reorder"})
        connection.send_result(msg_id)
    except KeyError:
        connection.send_error(
            msg_id,
            websocket_api.const.ERR_NOT_FOUND,
            "One or more item id(s) not found.",
        )
    except vol.Invalid as err:
        connection.send_error(msg_id, websocket_api.const.ERR_INVALID_FORMAT, f"{err}")
